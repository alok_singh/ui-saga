
export const FETCH_TAG_FILTERS = "FETCH_TAG_FILTERS";

export const ADD_TAG_FILTERS = "ADD_TAG_FILTERS";

export const DELETE_TAG_FILTERS = "DELETE_TAG_FILTERS";

export const INITIAL_TAG_STATE=[{"parent":"sc:audience","child":[{"name":"Adviser"}]},{"parent":"sc:channel","child":[{"name":"Web"}]},
    {"parent":"sc:product","child":[]},{"parent":"sc:feeStructure","child":[]},{"parent":"sc:statistic","child":[]},
    {"parent":"sc:feeStructureCharacteristic","child":[]},{"parent":"sc:materialCharacteristic","child":[]},
    {"parent":"sc:productCharacteristic","child":[]},{"parent":"sc:general","child":[]},
    {"parent":"sc:widget","child":[]}];



