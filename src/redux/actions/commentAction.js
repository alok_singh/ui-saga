import {FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS, FETCH_COMMENTS_FAILURE} from '../constants/commentConstant'
export const fetchCommentsRequest = () => {
    return {
      type: FETCH_COMMENTS_REQUEST,
      isFetching: true
    }
  }
  
  export const fetchCommentsSuccess = (payload) => {
    return {
      type: FETCH_COMMENTS_SUCCESS,
      payload
    }
  }
  
  export const fetchCommentsFailure = (error) => {
    return {
      type: FETCH_COMMENTS_FAILURE,
      isFetching: false,
      error
    }
  }