import {
  FETCH_ALL_FIELDS_REQUEST,
  FETCH_ALL_FIELDS_SUCCESS,
  FETCH_ALL_FIELDS_FAILURE,
  FETCH_GRAPHICS_SUCCESS,
  FETCH_BACKUPS_SUCCESS,
  FETCH_COPY_SUCCESS,
  ADD_GRAPHICS_ITEM_REQUEST,
  UPDATE_COPY_REQUEST,
  ADD_GRAPHICS_ITEM_SUCCESS,
  UPDATE_GRAPHICS_ITEM_REQUEST,
  ADD_BACKUPS_ITEM_REQUEST,
  UPDATE_BACKUPS_ITEM_SUCCESS,
  DELETE_GRAPHICS_ITEM_SUCCESS,
  DELETE_GRAPHICS_ITEM_FAILURE,
  DELETE_BACKUPS_ITEM_SUCCESS,
  DELETE_BACKUPS_ITEM_REQUEST,
  FETCH_BACKUPS_FAILURE
} from "../constants/writeConstants";
import _ from "lodash";
import * as R from "ramda";

let initialState = {
  loading: false,
  write: {},
  error: false
};
const writeReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_FIELDS_REQUEST:
      return { ...state, loading: true, error: false };
    case FETCH_ALL_FIELDS_SUCCESS:
      return {
        ...state,
        write: action.payload,
        loading: false,
        error: false
      };
    case FETCH_ALL_FIELDS_FAILURE:
      return {
        ...state,
        write: action.payload,
        loading: false,
        error: true
      };
    case FETCH_COPY_SUCCESS:
      return { ...state, copy: action.payload };
    case FETCH_GRAPHICS_SUCCESS:
      return { ...state, graphic: action.payload };
    case FETCH_BACKUPS_SUCCESS:
      return { ...state, backups: action.backupFields };
    case ADD_GRAPHICS_ITEM_REQUEST:
      state.write.graphics.push(action.payload);
      return state;
    case UPDATE_COPY_REQUEST:
      return updateCopyRequest(state, action);
    case UPDATE_GRAPHICS_ITEM_REQUEST:
      return updateGraphicRequest(state, action);
    case DELETE_GRAPHICS_ITEM_SUCCESS: {
      const newState = Object.assign({}, state);
      //const indexOfGraphicsToDelete = R.findIndex(R.propEq('a', action.payload.graphicId))(newState.write.graphics);
      const graphics=(graphic) => {
        console.log(graphic)
        return graphic.graphicId === action.payload.graphicId;
      }
      //const indexOfGraphicsToDelete = newState.write.graphics.findIndex(findGraphics);
      //console.log("Index",indexOfGraphicsToDelete)
      newState.write.graphics.splice(2, 1);
      return newState;
    }
    case ADD_BACKUPS_ITEM_REQUEST:
      state.write.backups.push(action.payload.backup);
      return state;
    case UPDATE_BACKUPS_ITEM_SUCCESS:
      return updateBackup(state, action);
	  case DELETE_BACKUPS_ITEM_SUCCESS:
      return deleteBackup(state,action);
    case FETCH_BACKUPS_FAILURE:
      return {...state,write_error:action.error}
    default:
      return state;
  }
};

export default writeReducer;

function updateCopyRequest(state, action) {
  const key = Object.keys(action.payload)[0];
  const index = _.findIndex(state.write.copy, { fieldId: parseInt(key) });
  state.write.copy[index].fieldValue = action.payload[key];
  return state;
}
function updateGraphicRequest(state, action) {
  let newState = _.cloneDeep(state);
  const key = Object.keys(action.payload)[0];
  _.remove(key, c => c == "graphicsId");
  const index = _.findIndex(newState.write.graphics, {
    graphicsId: action.payload.graphicsId
  });
  const itemIndex = _.findIndex(newState.write.graphics[index].fields, {
    fieldId: parseInt(key)
  });
  newState.write.graphics[index].fields[itemIndex].fieldValue =
    action.payload[key];
  return newState;
}
function updateBackup(state, action) {
  let newState = _.cloneDeep(state);
  const index = _.findIndex(newState.write.backups, {
    backupId: action.payload.backupId
  });
  newState.write.backups[index].name = action.payload.data.BackupName;
  newState.write.backups[index].backupId = action.payload.data.BackupId;
  return newState;
}
function deleteBackup(state,action){
  let newState = _.cloneDeep(state);
  const index = _.findIndex(newState.write.backups, {
    backupId: action.payload.backupId
  });
  newState.write.backups.splice(index,1)
  return newState
}
