
import {
    FETCH_COMMENTS_SUCCESS
} from '../constants/commentConstant';

let initialState = {
    isFetching: false
};
const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_SUCCESS:
            return {...state,comment:action.payload};
            break;
        default:
            return state;
    }
    
}

export default commentReducer;