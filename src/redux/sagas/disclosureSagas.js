import { put, takeLatest, call } from "redux-saga/effects";
import {
  FETCH_DISCLOSURE,
  FETCH_DISCLOSURE_FAILED
} from "../constants/disclosureConstants";

import {
  receiveDisclosureDetails,
  fetchErrorDisclosureDetails
} from "../actions/disclosureActions";
import { getDisclosureDetails } from "../../api/disclosureApi";

function* fetchDisclosureDetails(action) {
    try {
        const response = yield call(getDisclosureDetails,action.payload);

        yield put(receiveDisclosureDetails(response.data));

    } catch (error) {
        yield put(fetchErrorDisclosureDetails(error));
    }
}

function* disclosureSagas(action) {
    yield takeLatest(FETCH_DISCLOSURE, fetchDisclosureDetails);
}

export default disclosureSagas;