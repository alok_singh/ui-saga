// import saga helper function
import { put, takeLatest, call } from "redux-saga/effects";

// import action constant
import {
  FETCH_ALL_FIELDS_REQUEST,
  FETCH_GRAPHICS_REQUEST,
  DELETE_GRAPHICS_ITEM_REQUEST,
  FILE_UPLOAD_REQUEST,
  FILE_UPLOAD_SUCCESS,
  FETCH_BACKUPS_REQUEST,
  ADD_BACKUPS_ITEM_REQUEST,
  DELETE_BACKUPS_ITEM_REQUEST,
  FETCH_COPY_REQUEST,
  UPDATE_COPY_REQUEST,
  UPLOAD_COPY_FORM_DATA
} from "../constants/writeConstants";

// import action
import {
  fetchAllFieldsSuccess,
  fetchAllFieldsFailure,
  fetchGraphicsSuccess,
  fetchGraphicsFailure,
  fetchBackupsFailure,
  fetchBackupsSuccess,
  fetchCopySuccess,
  fetchCopyFailure,
  addGraphicsSuccess,
  deleteGraphicsRequestSuccess,
  deleteGraphicsRequestFailed,
  deleteBackupsItemSuccess,
  showErrorMsg,
  showSuccessMsg
} from "../actions/writeAction";

// import api
import {
  getWrite,
  fetchGraphics,
  deleteGraphics,
  addGraphics,
  uploadFileApi,
  updateGraphics,
  addbackupApi,
  deleteBackup,
  updateBackupApi,
  fetchCopyApi,
  saveWriteFormAPI
} from "../../api/writeApi";

function* fetchWrite(action) {
  console.log("writesags",action)
  try {
    const response = yield call(getWrite,action.payload);
    console.log("In saga", response.data);
    yield put(fetchAllFieldsSuccess(response.data));
  } catch (error) {
    yield put(fetchAllFieldsFailure(error));
  }
}

// fetch copy
function* fetchCopy() {
  try {
    const { data } = yield call(fetchCopyApi);
    yield put(fetchCopySuccess(data));
  } catch (error) {
    yield put(fetchCopyFailure(error));
  }
}

function* fetchGraphicsRequestSaga() {
  try {
    const response = yield call(fetchGraphics);
    yield put(fetchGraphicsSuccess(response.data));
  } catch (error) {
    yield put(fetchGraphicsFailure(error));
  }
}

function* deleteGraphic({ payload }) {
  try {
    let graphic = payload.graphics.filter(item=>item.graphicsId == payload.graphicId)[0]
    console.log(graphic,"graphicgraphic")
    if(graphic.name)yield call(deleteGraphics, payload);
    yield put(deleteGraphicsRequestSuccess(payload))
    yield put(showSuccessMsg("Delete Successfully"))
  } catch (error) {
    yield put(showErrorMsg("Error for deleting backup"));
  }
}

function* addGraphic({ payload, dispatch }) {
  try {
    yield put(addGraphicsSuccess(payload));
  } catch (error) {
    yield put(fetchGraphicsFailure(error));
  }
}
function* uploadFile({ payload }) {
  try {
    yield call(updateGraphics, payload);
    yield call(fetchGraphicsRequestSaga);
  } catch (error) {
    yield put(fetchGraphicsFailure(error));
  }
}


function* deletebackup({ payload }) {
  try {
    let backup = payload.backups.filter(item=>item.backupId == payload.backupId)
    if(backup.name)yield call(deleteBackup, payload);
    yield put(deleteBackupsItemSuccess(payload))
    yield put(showSuccessMsg("Delete Successfully"))
  } catch (error) {
    yield put(showErrorMsg("Error for deleting backup"));
  }
}

function* uploadCopyFormData({ payload }) {
  try {
    yield call(saveWriteFormAPI, payload);
  } catch (error) {
    console.log(error);
  }
}

function* writeSagas() {
  yield takeLatest(FETCH_ALL_FIELDS_REQUEST, fetchWrite);

  //yield takeLatest(FETCH_COPY_REQUEST,fetchCopy);

  yield takeLatest(FETCH_GRAPHICS_REQUEST, fetchGraphicsRequestSaga);
  yield takeLatest(DELETE_GRAPHICS_ITEM_REQUEST, deleteGraphic);
  yield takeLatest(FILE_UPLOAD_SUCCESS, uploadFile);

  yield takeLatest(DELETE_BACKUPS_ITEM_REQUEST, deletebackup);

  yield takeLatest(UPLOAD_COPY_FORM_DATA, uploadCopyFormData);
}

export default writeSagas;
