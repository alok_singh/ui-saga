import {
    put,
    takeLatest,
    call
} from "redux-saga/effects";
import {fetchCommentsSuccess, fetchCommentsFailure} from '../actions/commentAction'
import {getCommentApi} from '../../api/commentApi'
import { FETCH_COMMENTS_REQUEST } from "../constants/commentConstant";
function* fetchComment(){
    try {
        const {data} = yield call(getCommentApi)
        yield put(fetchCommentsSuccess(data))
    } catch (error) {
        yield put(fetchCommentsFailure(error))
    }
}
function* commentSagas() {
    yield takeLatest(FETCH_COMMENTS_REQUEST, fetchComment);
}

export default commentSagas;
