import React from "react";
import { Editor } from "slate-react";
import { Value } from "slate";
import GeHeader from "../shared/jsx/GeHeader";
import "../shared/scss/App.css";
import "../shared/scss/site.css";

// Create our initial value...
const initialValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: "block",
        type: "paragraph",
        nodes: [
          {
            object: "text",
            leaves: [
              {
                text: "A line of text in a paragraph."
              }
            ]
          }
        ]
      }
    ]
  }
});

class Home extends React.Component {
  saveComments() {
    console.log("The link was clicked.");
  }

  state = {
    value: initialValue
  };
  onChange = ({ value }) => {
    this.setState({ value });
  };

  onKeyDown = (event, change) => {
    if (event.key != "b" || !event.ctrlKey) return;
    event.preventDefault();
    change.toggleMark("bold");
    return true;
  };
  render() {
    return (
      <div className="App">
        <GeHeader />
      </div>
    );
  }

  renderMark = props => {
    switch (props.mark.type) {
      case "bold":
        return <strong {...props.attributes}>{props.children}</strong>;
    }
  };
}

export default Home;
