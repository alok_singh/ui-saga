import configDeterminator from "../configs/configDeterminator";
import axios from "axios";
import _ from "lodash";
export const getWrite = postId => {
  // let url = configDeterminator.cwbApiEndpoint + "/assignment/" + postId;
  let url="writeFields.json"; 
  return axios.get(url);
};
export const getCopyWrite = () => {
  let url = "http://localhost:4000/db"; //configDeterminator.cmpApiEndpoint + "/posts/post:41701029"
  return axios.get(url, {
    headers: {
      Accept: "application/json"
    }
  });
};
export const addCopyWrite = () => {
  let url = "http://localhost:4000/db"; //configDeterminator.cmpApiEndpoint + "/posts/post:41701029"
  return axios.get(url, {
    headers: {
      Accept: "application/json"
    }
  });
};

// get copy
export const fetchCopyApi = () => {
  return axios.get("http://localhost:4000/db");
};
// grphic action
export const fetchGraphics = () => {
  return axios.get("http://localhost:4000/graphicFields");
};
export const deleteGraphics = ({ graphicId }) => {
  return axios.delete(configDeterminator.cwbApiEndpoint + "/graphics/" + graphicId);
};

export const uploadFileApi = ({ file, url = "/graphics" ,assignmentId, userId}) => {
  const formData = new FormData();
  formData.append("file", file);
  const config = { headers: { "content-type": "multipart/form-data" } };
  formData.append("userId", userId);
  formData.append("postId", assignmentId);
  console.log(formData);
  return axios.post(configDeterminator.cwbApiEndpoint + url, formData);
};
export const updateGraphics = ({ data, graphic, graphicId }) => {
  const graphicItem = _.filter(graphic, { id: graphicId })[0];
  graphicItem.fileName = data.filename;
  graphicItem.originalFileName = data.originalname;
  graphicItem.filePath = data.path;
  return axios.put(
    `http://localhost:4000/graphicFields/${graphicId}`,
    graphicItem
  );
};


export const deleteBackup = ({ backupId, backup }) => {
  return axios.delete(configDeterminator.cwbApiEndpoint + "/backups/" + backupId);
};
export const updateBackupApi = ({ data, backupId, backups }) => {
  const backupItem = _.filter(backups, { id: backupId })[0];
  backupItem.fileName = data.filename;
  backupItem.originalFileName = data.originalname;
  backupItem.filePath = data.path;
  return axios.put(
    `http://localhost:4000/backupFields/${backupId}`,
    backupItem
  );
};
export const saveWriteFormAPI = ({ write,user }) => {
  return axios.put(
    configDeterminator.cwbApiEndpoint + "/assignment/"+user.assignment.id,
    write.write
  );
};
