
import axios from "axios";

export const getDisclosureDetails = (param) => {
  return axios
    .post("/services/api/v1/disclosure/digPreview?"+param, {
      headers: {
        Accept: "application/json"
      }
    })
};
