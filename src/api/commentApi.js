import configDeterminator from "../configs/configDeterminator";
import axios from "axios";
import _ from 'lodash'

export const getCommentApi = () => {
    return axios.get('http://localhost:4000/comments')
};