import React from "react";
import Popup from "reactjs-popup";
import "./../../assets/scss/modals.css";

const ShareWithOthers = () => (
  <div className="example-warper">
    <Popup
      trigger={<a className="btn btn-link">Share With Others</a>}
      position="bottom center"
      on="click"
    >
      <Card title="Bottom Center" />
    </Popup>
  </div>
);
const Card = ({ title }) => (
  <div className="card">
    <div className="content">
      <span>Anyone with this link can view and edit</span>
      <br />
      <a className="percolate">http://capitalgroup.com/sdeq0912/comment</a>
      <br />
      <br />

      <span>Anyone with this link can view and edit</span>
      <br />
      <a
        className="percolate"
        href="http://cg-wwb-frontend-prototypes.s3-website-us-west-2.amazonaws.com/document/123"
        target="_blank"
      >
        http://capitalgroup.com/sdeq0912/comment
      </a>
    </div>
  </div>
);

export default ShareWithOthers;
