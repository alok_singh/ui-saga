import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CameraIcon from "@material-ui/icons/PhotoCamera";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import ReactLoading from "react-loading";
import * as R from "ramda";
import { withRouter, Route, Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Moment from "moment";

const styles = theme => ({
  appBar: {
    position: "relative"
  },
  icon: {
    marginRight: theme.spacing.unit * 2
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper
  },
  heroContent: {
    maxWidth: 600,
    margin: "0 auto",
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  fabButton: {
    margin: theme.spacing.unit,
    color: "#424242",
    backgroundColor: "#add8e6",
    height: "35px",
    minWidth: "32px",
    width: "35px"
  }
});

function DashBoard(props) {
  const { allAssignments, loading, error, userDetails } = props.assignments;
  const { classes } = props;
  if (loading)
    return (
      <div>
        <ReactLoading type={"bubbles"} color={"#000000"} />
      </div>
    );
  if (error) return <div>Error while fetching data </div>;
  if (R.isEmpty(allAssignments)) return "";
  if (R.isEmpty(allAssignments) || R.isEmpty(allAssignments.assignments))
    return (
      <div className={classNames(classes.layout, classes.cardGrid)}>
        <main>
          {" "}
          <Grid container spacing={40}>
            <Grid item xs={12}>
              <Typography gutterBottom variant="headline" component="h2">
                There is no content assigned to you.
              </Typography>
              <Typography>
                {" "}
                Any content that is assigned to you will appear on your
                homepage.
              </Typography>
            </Grid>
          </Grid>
        </main>
      </div>
    );

  return (
    <div>
      <main>
        <div className={classNames(classes.layout, classes.cardGrid)}>
          {/* End hero unit */}
          <Grid container spacing={24}>
            {Object.keys(
              allAssignments.assignments
            ).map((assignment, index) => {
              return (
                <Grid item key={index} sm={6} md={4} lg={3}>
                  <Card className={classes.card}>
                    <CardHeader
                      avatar={
                        <Avatar aria-label="Recipe" className={classes.fabButton}>
                          {userDetails.user}
                        </Avatar>
                      }
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="title">
                        {allAssignments.assignments[assignment].title}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        component={Link}
                        to={"/" + allAssignments.assignments[assignment].id}
                        size="small"
                        color="primary"
                      >
                        View
                      </Button>
                      <Typography>
                        {Moment(
                          allAssignments.assignments[assignment].liveAt
                        ).format("MMMM Do, YYYY")}
                      </Typography>
                    </CardActions>
                  </Card>
                </Grid>
              );
            })}
          </Grid>
        </div>
      </main>
    </div>
  );
}

DashBoard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect()(withStyles(styles)(DashBoard));
