import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import PageNameField from "./PageNameField";
import orange from "@material-ui/core/colors/orange";
import classNames from "classnames";
import CWBTabs from "../Tabs/CWBTabs/Tabs";
import { connect } from "react-redux";
import { uploadCopyFormData } from "../../redux/actions/writeAction";
const styles = theme => ({
  root: {
    flexGrow: 1
  },
  appBar: {
    backgroundColor: "#262d3a",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  navButton: {
    margin: theme.spacing.unit,
    color: "#FFFFFF",
    fontSize: "12px",
    textTransform: "capitalize"
  },
  fabButton: {
    margin: theme.spacing.unit,
    color: "#424242",
    backgroundColor: "#add8e6",
    height: "32px",
    minWidth: "32px",
    width: "32px"
  },
  toolbar: {
    padding: 0
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  toolbarTitle: {
    flex: 1
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor: orange[500],
    "&:hover": {
      backgroundColor: orange[700]
    }
  }
});

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.saveTheForm = this.saveTheForm.bind(this);
  }
  handleDrawerOpen = () => {
    this.setState({ open: !this.state.open });
  };
  state = {
    open: false
  };
  saveTheForm() {
    let url = window.location.pathname.split("/")[1];
    switch (url) {
      case "write":
        this.props.uploadCopyFormData(this.props);
        break;
      default:
        console.log("Not to Save Form");
    }
  }
  render() {

    let { user, dashBoardLoaded ,assignment } = this.props.user;
    console.log("user data",this.props)
    const { classes } = this.props;
    return (
      <Fragment>
        <AppBar
          position="fixed"
          className={classNames(
            classes.appBar,
            this.state.open && classes.appBarShift
          )}
        >
          <Grid container direction="row" justify="center">
            <Grid item lg={12}>
              <Toolbar className={classes.toolbar}>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(
                    classes.menuButton,
                    this.state.open && classes.hide
                  )}
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="subheading" color="inherit">Creative WorkBench:</Typography>
                <Typography
                  variant="subheading"
                  color="inherit"
                  noWrap
                  className={classes.toolbarTitle}
                >
                  <PageNameField assignmentDetails={this.props} />
                </Typography>

                <Button
                  variant="contained"
                  className={classes.button}
                  onClick={this.saveTheForm}
                  disabled={dashBoardLoaded}
                >
                  Save
                </Button>
                {""}
                <Button
                  variant="fab"
                  size="small"
                  className={classes.fabButton}
                >
                  {user}
                </Button>
              </Toolbar>
            </Grid>
          </Grid>
        </AppBar>
        <CWBTabs hide={dashBoardLoaded} open={this.state.open} />
      </Fragment>
    );
  }
}

TopBar.propTypes = { classes: PropTypes.object.isRequired };
const mapStateToProps = state => {
  return {
    write: state.write
  };
};
export default connect(mapStateToProps, { uploadCopyFormData })(
  withStyles(styles)(TopBar)
);
