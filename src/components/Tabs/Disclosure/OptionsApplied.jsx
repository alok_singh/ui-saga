import React  from 'react';
import './../../../assets/scss/options.css';
import PropTypes from "prop-types";
import { connect } from "react-redux";


class OptionsApplied extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            storeArray:[]
        };
        this.fetchFilterState=this.fetchFilterState.bind(this);  
    }
    fetchFilterState(){
        var newArray = [];
        this.setState({storeArray:[]});
        Object.keys(this.props.tagArray).map((item)=>{
            if(this.props.tagArray[item].parent==this.props.contextValue){
                this.props.tagArray[item].child.map((items)=>
                {  
                    newArray.push(items.name);   

                }

           )
            }this.setState({storeArray:newArray});
           
        }) } 
    componentDidMount(){
        this.fetchFilterState();
    }
    
    componentWillReceiveProps(){
        this.fetchFilterState();   
    }
  
    render(){
       
        return (
   
            <div>
                  <div className="options-header"><span>Options Applied</span></div>
                <div className="parent-text">
                  {this.state.storeArray && this.state.storeArray.map((items,index)=>
                      <a className="anchor-text" key={index} color="primary"  style={{ marginBottom: '1rem' }}>{items}</a>
                 ) }
          
                  </div>
            </div>
        );
                  } }

const mapStateToProps = state => {
    return {
        tagArray: state.filters.tagFilter
    };
};

export default connect(mapStateToProps)(OptionsApplied);



