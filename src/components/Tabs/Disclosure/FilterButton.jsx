import React from "react";
import "./../../../assets/scss/options.scss";
import less from "../../../assets/images/Disclosure/less.png";
import Button from "@material-ui/core/Button";
import CancelIcon from "@material-ui/icons/Cancel";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchTagFiltersDetails,deleteTagFilter } from "../../../redux/actions/tagFilterActions";

const styles = theme => ({
    container: {
        display: "flex",
        flexWrap: "wrap"
    },
    margin: {
        margin: theme.spacing.unit
    },

    cssFocused: {},

    bootstrapRoot: {
        padding: 0,
        "label + &": {
            marginTop: theme.spacing.unit * 3
        }
    },
    bootstrapInput: {
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "10px 12px",
        width: "80%",
        margin: "0px 0px",
        transition: theme.transitions.create(["border-color", "box-shadow"]),
        fontFamily: [
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          "Roboto",
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"'
        ].join(","),
        "&:focus": {
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
        }
    },
    bootstrapFormLabel: {
        fontSize: 18
    }
});

class FilterButton extends React.Component  {
    constructor(props) {
        super(props);
        this.handleCancle=this.handleCancle.bind(this);
    }

    handleCancle(e){
        console.log("cancel button clicked",e.target.id);
        if( e.target.parentElement.parentElement.type=='button'){
            e.target.parentElement.parentElement.style.display='none'; 
        }else{
            e.target.parentElement.parentElement.parentElement.style.display='none'; 
        }    
    }
   
    render(){

        return ( <React.Fragment>{this.props.storeArray && this.props.storeArray.map((items,index)=>
            <Button  key={index}
                               variant="contained"
                               color="primary"
                               classes={{
                               root: "classes-state-btn-aud"
                                 }}>
    {items}
<CancelIcon id={items} onClick={this.handleCancle}/>
</Button>)
    }        </React.Fragment>
);
    }}
         

export default FilterButton;

