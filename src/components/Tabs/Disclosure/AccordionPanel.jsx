import React from "react";
import "./../../../assets/scss/options.scss";
import less from "../../../assets/images/Disclosure/less.png";
import AddIcon from "@material-ui/icons/AddCircle";
import SearchIcon from "@material-ui/icons/Search";
import CancelIcon from "@material-ui/icons/Cancel";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import FilterButton from "./FilterButton";
import OptionsAvailable from './OptionsAvailable';
import OptionsApplied from './OptionsApplied';
import DeleteIcon from  '@material-ui/icons/Delete';
import compose from 'recompose/compose';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchTagFiltersDetails,addTagFilter,deleteTagFilter } from "../../../redux/actions/tagFilterActions";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';

const styles = theme => ({
    container: {
        display: "flex",
        flexWrap: "wrap"
    },
    margin: {
        margin: theme.spacing.unit
    },

    cssFocused: {},

    bootstrapRoot: {
        padding: 0,
        "label + &": {
            marginTop: theme.spacing.unit * 3
        }
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
    bootstrapInput: {
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "10px 12px",
        width: "80%",
        margin: "0px 0px",
        transition: theme.transitions.create(["border-color", "box-shadow"]),
        fontFamily: [
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          "Roboto",
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"'
        ].join(","),
        "&:focus": {
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
        }
    },
    bootstrapFormLabel: {
        fontSize: 18
    }
});

class AccordionPanel extends React.Component  {
    constructor(props) {
        super(props);
	  
        this.state = {name:'',
            tagArr: [],
            value:'',
            audienceRef:'',
            tagFilterArr: [],
            storeArray:[],
			open: false,
        };
	 
        this.handleAddClick = this.handleAddClick.bind(this); 
        this.handleSearchClick = this.handleSearchClick.bind(this); 
        this.onChangeHandler= this.onChangeHandler.bind(this);
        this.handleInputClick= this.handleInputClick.bind(this);
        this.fetchFilterState=this.fetchFilterState.bind(this); 
    }
	 handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
    fetchFilterState(){
        let newArray = [];
        Object.keys(this.props.tagArray).map((item)=>{
            if(this.props.tagArray[item].parent==this.props.parentId){
                this.props.tagArray[item].child.map((items)=>
                {  
                    newArray.push(items.name);   

                }

           )
            }
            this.setState({storeArray:newArray});
           
        })
    }
    componentDidMount(){
        if(this.props.parentId!==""){
            let constructedUrl = '/bin/DebugToolServlet?tagId='+this.props.parentId;
            fetch(constructedUrl,{
                method: 'GET',
                mode:'cors',
                headers: {
                    Accept: 'application/json'
                },
            }).then(results => {
                return results.json();
            }).then(dataValues => { let tags = dataValues.data.map((tag) => {
                this.state.tagArr.push(tag.text);

            })});
        }
        this.fetchFilterState();  
    }
 
    handleAddClick(e){
        let updateArr=this.props.tagArray;
        let contextValue=this.state.name;
        let parentContext=this.props.parentId;
        if(contextValue!=''){
            updateArr.forEach(function iter(a) {
                if (parentContext.includes(a.parent)) {
                    let tempArr={"name":contextValue};
                    a.child = a.child.concat(tempArr);
                }
           
            });

        this.props.addTagFilter(updateArr);
        this.setState({name:''});
        }else{
            alert("Please select Tag");
        }
        this.fetchFilterState();
    }
   
   
    handleSearchClick(){
       console.log("Search clicked"); 
        //this.setState({ open: true });
    }
    onChangeHandler(e){
       
        this.setState({
            input: e.target.value
        });
        this.setState({
            name: e.target.value
        });
		
        this.state.value = this.state.tagArr
            .filter(d=>this.state.input === '' || d.includes(this.state.input)).map((d, index) => <li  value={d} key={index}>{d}</li>);
                return this.state.value;
            }
    handleInputClick(e){
        
        this.setState({
            name: e.target.innerHTML
        });
        this.setState({
            value: ''
        });
    }
  
    render(){
        const { classes } = this.props;
        const { fullScreen } = this.props;		
        return (
  
          <div>
           <div>
        
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
          <DialogContent>
            <div className="row options">
      
      <div className="col-md-5 available">
      <OptionsAvailable contextValue={this.state.contextValue}/>
      </div>
      <div className="col-md-2 Appliedbuttons">
      <Button variant="contained" color="primary"  >
        <AddIcon />
      Add

    </Button><br/><br/>
    <Button variant="contained" color="primary"  >
        <DeleteIcon />
      Delete

    </Button>
      </div>
   
      <div className="col-md-5 applied">
      <OptionsApplied contextValue={this.state.contextValue}/>
      </div>


</div>

 <div className="row btns">
 <Button variant="contained" color="primary"  >
        <AddIcon />
      Save

    </Button>
    <Button variant="contained" color="primary" onClick={this.handleClose} classes={{
        root: 'classes-state-root-btn-cancel',
    }
                }  >
        <CancelIcon />
      Cancel

    </Button>
      </div>
          </DialogContent>
          
        </Dialog>
      </div>
            <div className="input-group">
              <TextField onChange={this.onChangeHandler}
        value={this.state.name}
        label=""
        id="bootstrap-input"
        InputProps={{
            disableUnderline: true,
            classes: {
                root: classes.bootstrapRoot,
                input: classes.bootstrapInput
            }
        }}
    InputLabelProps={{
        shrink: true,
        className: classes.bootstrapFormLabel
    }}/><ul onClick={this.handleInputClick}>{this.state.value}</ul>
  <AddIcon onClick={this.handleAddClick} /> <SearchIcon onClick={this.handleClickOpen}/>
</div>
<div className="aud-buttons">
  <FilterButton storeArray={this.state.storeArray}/>
</div>
</div>
  );
}
}

const mapStateToProps = state => {
    return {
        tagArray: state.filters.tagFilter 
    };
};

  const mapDispatchToProps = dispatch => ({
      addTagFilter: (name) => dispatch(addTagFilter(name))
  });

  AccordionPanel.propTypes = {
      addTagFilter: PropTypes.func.isRequired,
	  fullScreen: PropTypes.bool.isRequired,
  };

export default compose(withStyles(styles),withMobileDialog(),connect(mapStateToProps, mapDispatchToProps))(AccordionPanel);


