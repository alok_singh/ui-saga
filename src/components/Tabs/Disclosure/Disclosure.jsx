import React from 'react';
import "../../../shared/scss/Disclosure.css";
import DisclosureList from './DisclosureList';
import OptionsAvailable from './OptionsAvailable';
import OptionsApplied from './OptionsApplied';
import AccordionPanel from './AccordionPanel';
import AddIcon from '@material-ui/icons/AddCircle';
import DeleteIcon from  '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import CancelIcon from '@material-ui/icons/Cancel';
import PropTypes from "prop-types";
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchDisclosureDetails } from "../../../redux/actions/disclosureActions";
import {addTagFilter} from "../../../redux/actions/tagFilterActions";


class Disclosure extends React.Component  {
    constructor(props) {
        super(props);
        this.state={
            accordionValue:[{"key":"sc:audience","title":"Audience"},{"key":"sc:channel","title":"Channel"},
                {"key":"sc:product","title":"Product"},{"key":"sc:feeStructure","title":"Fee Structure"},
                {"key":"sc:statistic","title":"Statistic"},{"key":"sc:feeStructureCharacteristic","title":"Fee Structure Characteristic"},
                {"key":"sc:materialCharacteristic","title":"Material Characteristic"},{"key":"sc:productCharacteristic","title":"Product Characteristic"},
                {"key":"sc:general","title":"General"},{"key":"","title":"Widget Node Name (Under Construction)"}],
            tagFilterArr:[],
            contextValue:'sc:audience',	
        };
        this.handleClick=this.handleClick.bind(this);
        this.handleResetClick=this.handleResetClick.bind(this);
        this.handleAccordionClick=this.handleAccordionClick.bind(this);
        this.addPercolateTag=this.addPercolateTag.bind(this);
		
    
    }

    componentDidMount(){
        //this.addPercolateTag();
    }
   
    handleAccordionClick(e){ 
        this.setState({contextValue:e});         
    }
    handleClick(){
       
        let param='';
        Object.keys(this.props.tagFilter).map((item,index)=>{
            if(index==0){
                param=this.props.tagFilter[item].parent.replace('sc:','')+"=";
            }else{
                param=param+"&"+this.props.tagFilter[item].parent.replace('sc:','')+"=";
            }
           this.props.tagFilter[item].child.map((items,index)=>
           {  
               if(index==0) {    
                   param=param+items.name;
               }else{
                   param=param+","+items.name;
               }
           }
            
           )
            
           
        })
        this.props.fetchDisclosureDetails(param); 
    }
    addPercolateTag(){
        let audiencePercolateTag={"name":"advisor"};
        let channelPercolateTag={"name":"web"};
        this.props.assignment.taxonomy && this.props.assignment.taxonomy.map((items)=>{
            
            if(items=="Audience"){
                
                items.map((tag,index)=>{
                    audiencePercolateTag='';
                    if(index==0){
                        audiencePercolateTag={"name":tag};
                    }else{
                        audiencePercolateTag= audiencePercolateTag+","+{"name":tag};
                    }
                })
               
            }else if (items=="Channel"){
                items.map((tag,index)=>{
                    channelPercolateTag='';
                    if(index==0){
                        channelPercolateTag={"name":tag};
                    }else{
                        channelPercolateTag= channelPercolateTag+","+{"name":tag};
                    }
                })
            }
        })
        let initialState=[{"parent":"sc:audience","child":[audiencePercolateTag]},{"parent":"sc:channel","child":[channelPercolateTag]},
    {"parent":"sc:product","child":[]},{"parent":"sc:feeStructure","child":[]},{"parent":"sc:statistic","child":[]},
    {"parent":"sc:feeStructureCharacteristic","child":[]},{"parent":"sc:materialCharacteristic","child":[]},
    {"parent":"sc:productCharacteristic","child":[]},{"parent":"sc:general","child":[]},
    {"parent":"sc:widget","child":[]}];
        this.props.addTagFilter(initialState);
    }
    handleResetClick(){
		let emptyArray=[];
		 this.setState({
            tagFilterArr: emptyArray
        });
        this.addPercolateTag();
		
    }
    render(){
 
        return(
<div>
    <div className="Add-Disclosure">Add Disclosure</div>
    <div className="Search-for-related-d">Search for related disclosure on the left  |  Review disclosure results on the right
If you need to amend the list, edit the topics. After you are done, select 	&quot;Finish and Apply to Draft&quot;</div>
    <div className="Rectangle">
        <div className="Shape"></div>
         <div className="Disclosure-Search">Disclosure Search</div>
     </div>
<div className="row">
     
<div className="Rectangle-6-Acc">
    {this.state.accordionValue.map((items,index) =>
        <ExpansionPanel  key={items.key} onClick={() => this.handleAccordionClick(items.key)} >
            <ExpansionPanelSummary  expandIcon={<ExpandMoreIcon />} className="Rectangle-6">
              {items.title}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <AccordionPanel parentId={items.key} tagFilterArr={this.state.tagFilterArr}/>
            </ExpansionPanelDetails>
      </ExpansionPanel>
     
   )}  
<br/>

    <Button variant="contained" className="TopBar-button-11"   onClick={this.handleResetClick}>Reset</Button>
    <Button variant="contained" className="TopBar-button-11"   onClick={this.handleClick}>Preview</Button>

</div>
<div className="col-md-9">
<div className="row">
<div className="Rectangle-12"><p className="Disclosure-Analyzer">Disclosure Analyzer Results</p></div>
</div>
     
<div className="row disclosure">
   <DisclosureList/>
                                
           </div>    
</div>
           </div>
</div>    
);
        }
}

const mapStateToProps = state => {
    return {
        loading: state.disclosures.loading,
        disclosure: state.disclosures.disclosure,
        error: state.disclosures.error,
        tagFilter:state.filters.tagFilter,
        assignment: state.assignments.assignment
    };
};

const mapDispatchToProps = dispatch => ({
    fetchDisclosureDetails: (param) => dispatch(fetchDisclosureDetails(param)),
    addTagFilter: (name) => dispatch(addTagFilter(name))
});

Disclosure.propTypes = {
    fetchDisclosureDetails: PropTypes.func.isRequired,
    addTagFilter: PropTypes.func.isRequired,
	
};


export default connect(mapStateToProps, mapDispatchToProps)(
  Disclosure
);




