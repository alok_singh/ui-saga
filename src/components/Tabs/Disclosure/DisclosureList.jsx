import React from 'react';
import Button from '@material-ui/core/Button';
import { connect } from "react-redux";
import PropTypes from "prop-types";


class DisclosureList extends React.Component  {
    constructor(props) {
        super(props);

    }

    render(){
        const disclosureItems=this.props.state.disclosures.disclosure.Results;
        return (
          <div>
              <div className="">
                  
        {disclosureItems && disclosureItems.map((disclosure,index) =>
            <div key={index+1} className="">
            <br/>
                <div className="row">
                  
                  
                  <div   dangerouslySetInnerHTML={{ __html: disclosure.language }} />
                      <br/>
                      <div className="float-right">
                      <Button variant="contained" className="Finish">
						Finish and Apply to draft
					  </Button>
					 
                   
                      </div>
                      </div>
					  </div>


			)}
        </div>
                          </div>

);}
}
const mapStateToProps = state => {
    return {
        state
    };
};

export default connect(mapStateToProps)(DisclosureList);
