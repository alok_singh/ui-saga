import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import './../../../assets/scss/options.css';
import AddIcon from '@material-ui/icons/AddCircle';


class OptionsAvailable extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { collapse: false };
    }
  
    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
  
    render() {
        return (
          <div>
              <div className="options-header"><span>Options Available</span></div>
          <div className="parent-text">
            {/*<a className="anchor-text" href="#" color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>+Audience</a>*/}
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> Audience</a>
            <div className="child-text">
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> Advisor</a>
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> Institional</a>
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> Internal Associate</a>
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> Investor</a>
            <a className="anchor-text" href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> RIA</a>
            <a className="anchor-text last"  href="#" color="primary"  style={{ marginBottom: '1rem' }}><AddIcon/> RP Participants</a>

            </div>
            </div>
            <Collapse isOpen={this.state.collapse}>
              <Card>
                <CardBody>
                Anim pariatur cliche reprehenderit,
                 enim eiusmod high life accusamus terry richardson ad squid. Nihil
                anim keffiyeh helvetica, craft beer labore wes anderson cred
                nesciunt sapiente ea proident.
               </CardBody>
             </Card>
           </Collapse>
         </div>
        );
            }
}
  


export default OptionsAvailable;
