import React from "react";

const Back_Ups = () => (
  <div>
    <span>
      <b>Backups</b>
    </span>
    <br />
    <br />
    <div className="row">
      <div className="col-md-3 backUp">
        <span>Backup File 1</span>{" "}
        <span style={{ float: "right" }}>Delete</span>
        <br />
        <br />
        <input type="file" />
        <br />
        <br />
        <span>LINKED TO</span>
        <br />
        <br />
        <span>
          That means the remaining $56,000 contribution is taken from the
          client's lifetime exemption
        </span>
      </div>

      <div className="col-md-3 backUp">
        <span>Backup File 2</span>{" "}
        <span style={{ float: "right" }}>Delete</span>
        <br />
        <br />
        <input type="file" />
        <br />
        <br />
        <span>LINKED TO</span>
        <br />
        <br />
        <span>
          That means the remaining $56,000 contribution is taken from the
          client's lifetime exemption
        </span>
      </div>

      <div className="col-md-3 backUp">
        <span>Backup File 3</span>{" "}
        <span style={{ float: "right" }}>Delete</span>
        <br />
        <br />
        <input type="file" />
        <br />
        <br />
        <span>LINKED TO</span>
        <br />
        <br />
        <span>
          That means the remaining $56,000 contribution is taken from the
          client's lifetime exemption
        </span>
      </div>
    </div>
  </div>
);

export default Back_Ups;
