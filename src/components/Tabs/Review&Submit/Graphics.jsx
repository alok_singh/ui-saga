import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { GridList, GridListTile, Typography } from "@material-ui/core";
const styles = theme => {
  return {
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "flex-start",
      overflow: "hidden",
      paddingLeft: "24px"
    }
  };
};

const Graphics = props => {
  const { classes, data } = props;
  return (
    <div className={classes.root}>
      {data
        .filter(graphic => graphic.graphicsId != "")
        .map((graphic, index) => (
          <GridList cellHeight={50} className={classes.gridList} cols={1}>
            <GridListTile key={graphic.graphicsId} col={1}>
              <Typography component="h3">Graphics #{index + 1}</Typography>
            </GridListTile>
            {graphic.fields.map(item => (
              <GridListTile key={item.img} cols={1}>
                <Typography component="h3">{item.fieldName}</Typography>
                <Typography component="span">
                  {item.fieldValue || "N/A"}
                </Typography>
              </GridListTile>
            ))}
          </GridList>
        ))}
    </div>
  );
};
export default withStyles(styles)(Graphics);
