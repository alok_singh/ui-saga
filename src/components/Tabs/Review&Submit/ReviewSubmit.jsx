import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Toolbar, Typography, Button, Grid } from "@material-ui/core";
import { connect } from "react-redux";
import Summary from "./Summary";
import Back_Ups from "./Back_Ups";
import DisclosureSummary from "./DisclosureSummary";
import Graphics from "./Graphics";

const styles = theme => {
  return {
    root: {},
    heading: {
      fontWeight: 300,
      color: "#255d63",
      fontSize: "28px"
    },
    subheading: {
      ...theme.mixins.gutters(),
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      backgroundColor: "#def6f9",
      flexGrow: 1,
      position: "relative",
      fontSize: "16px",
      fontWeight: "900"
    },
    reviewContent: {
      color: "#000000",
      fontSize: "16px",
      fontWeight: "normal",
      fontfamily: "Avenir-Roman"
    },
    button: {
      width: "100%",
      background: "#73c0c9",
      color: "#fff",
      textTransform: "capitalize",
      fontWeight: "900",
      marginBottom: "0.4rem"
    }
  };
};

const ReviewSubmit = props => {
  const { classes, write } = props;
  console.log("writewrite", write);
  return (
    <div className={classes.root}>
      <Toolbar>
        <Typography variant="title" className={classes.heading}>
          Review & Submit
        </Typography>
      </Toolbar>
      <Toolbar>
        <Typography
          varian="display4"
          className={classes.reviewContent}
          component="span"
        >
          Review your work.Go back to any step to edit inputs as needed. When
          finished, download files for submission to WorkFront.
        </Typography>
      </Toolbar>
      <Grid container>
        <Grid xs={9}>
          <Toolbar>
            <Typography
              variant="inherit"
              className={classes.subheading}
              component="h2"
            >
              Draft
            </Typography>
          </Toolbar>
        </Grid>
        <Grid xs={3}>
          <Grid xs={12}>
            <Button variant="contained" className={classes.button}>
              Print
            </Button>
          </Grid>
          <Grid xs={12}>
            <Button variant="contained" className={classes.button}>
              DownLoad Zip File
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {write.copy && <Summary data={write.copy} />}
      {write.graphics && <Graphics data={write.graphics} />}
      <Back_Ups />
      <DisclosureSummary />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    write: state.write.write
  };
};

export default withStyles(styles)(connect(mapStateToProps, null)(ReviewSubmit));
