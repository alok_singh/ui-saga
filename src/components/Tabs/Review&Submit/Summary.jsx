import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Toolbar, Typography, Button, Grid } from "@material-ui/core";
const styles = theme => {
  return {
    grid: {
      paddingLeft: "24px",
      marginBottom: "24px"
    }
  };
};
const Summary = props => (
  <Fragment>
    {props.data.map(copy => {
      return (
        <Grid xs={11} className={props.classes.grid}>
          <Typography component="h3">{copy.fieldName}</Typography>
          <Typography component="span">{copy.fieldValue || "N/A"}</Typography>
        </Grid>
      );
    })}
  </Fragment>
);
export default withStyles(styles)(Summary);
