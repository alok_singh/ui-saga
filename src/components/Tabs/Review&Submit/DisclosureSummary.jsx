import React from "react";

const DisclosureSummary = () => (
  <div>
    <span>
      <b>Disclosure</b>
    </span>
    <br />
    <br />

    <div className="row">
      <div className="col-md-10">
        1. Investments are not FDIC-insured, nor are they deposits of guaranteed
        by a bank or any other entity so they may lose value.
      </div>
    </div>
    <br />
    <br />
    <div className="row">
      <div className="col-md-10">
        2. Investments are not FDIC-insured, nor are they deposits of guaranteed
        by a bank or any other entity so they may lose value.
      </div>
    </div>
  </div>
);

export default DisclosureSummary;
