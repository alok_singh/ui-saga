import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import BackupsItem from "./BackupsItem";
import SectionHeader from "../SectionHeader";
import AddBackupsItem from "./AddBackupsItem";
import { uploadFileApi } from "../../../../api/writeApi";
import _ from "lodash";
import * as backups_actions from "../../../../redux/actions/writeAction";
// import * as fileupload_actions from '../../../actions/fileupload_actions';

const styles = theme => {
  return {
    root: {
      flexGrow: 1
    },
    backupitems: {
      display: "flex",
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      margin: "20px 15px"
    }
  };
};

class Backups extends React.Component {
  state={
    loading:false
  }
  handleAddNewBackup = () => {
    const { backups } = this.props;
    let backup = backups[0];
    backup = _.cloneDeep(backup);
    backup.backupId = Date.now();
    backup.name = "";
    this.props.showSuccessMsg("New Backup Added")
    this.props.addBackupsItemRequest({ backup });
  };
  onFileChange = (backupId, fieldName, file) => {
    let assignmentId = this.props.store.assignments.assignment.id;
    let userId = "INYMNB";
    this.setState({loading:true})
    uploadFileApi({ file, url: "/backups" ,assignmentId,userId}).then(({ data }) => {
      this.setState({loading:false})
      this.props.showSuccessMsg("Successfully Uploaded")
      this.props.uploadBackupSuccess({ data, backupId });
    }).catch(error=>{
      this.setState({loading:false})
      this.props.showErrorMsg("File Upload Fail")
    });
  };
  handleDeleteBackupItem = backupId => {
    const { backups } = this.props;
    this.props.deleteBackupsItemRequest({ backupId, backups });
  };
  renderItems = () => {
    const { backups } = this.props;
    if (!backups) {
      return;
    }
    return backups
      .filter(backup => backup.backupId != "")
      .map((backupItem, index) => {
        const backupField = backupItem;
        return (  
          <BackupsItem
            loading={this.state.loading}
            key={index}
            id={backupItem.backupid}
            fileName={backupItem.Name}
            fieldProps={backupField}
            onFileChange={this.onFileChange}
            onDeleteClick={this.handleDeleteBackupItem}
          />
        );
      });
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item xs={12}>
            <SectionHeader title="Backups" />
            <div className={classes.backupitems}>
              {this.renderItems()}
              <AddBackupsItem onAddBackupClick={this.handleAddNewBackup} />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state,
    backups: state.write.write.backups
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(backups_actions, dispatch)
  };
};

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(
  Backups
);
export default withStyles(styles)(ConnectedComponent);
