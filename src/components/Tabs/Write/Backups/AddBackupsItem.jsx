import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const styles = theme => {
  return {
    grid: {
      display: "flex",
      flex: 1,
      alignItems: "center",
      margin: "10px"
    },
    card: {
      width: 350,
      height: 250,
      backgroundColor: "#f9fafa",
      padding: "10px",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      marginBottom: "15px",
      cursor: "pointer"
    }
  };
};

class AddBackupsItem extends React.Component {
  onClick = () => {
    if (this.props.onAddBackupClick) {
      this.props.onAddBackupClick();
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid item className={classes.grid}>
        <Card className={classes.card} onClick={this.onClick}>
          <span>+</span>
          <span>Add New Backup</span>
        </Card>
      </Grid>
    );
  }
}

export default withStyles(styles)(AddBackupsItem);
