import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import FormField from "../../../Form/FormField/FormField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ReactLoading from "react-loading";

const styles = theme => {
  return {
    grid: {
      display: "flex",
      flex: 1,
      alignItems: "center",
      margin: "10px"
    },
    header: {
      padding: "10px"
    },
    content: {
      padding: "10px"
    },
    card: {
      width: 350,
      height: 250,
      backgroundColor: "#f9fafa",
      padding: "10px",
      marginBottom: "15px"
    },
    blockquote: {
      borderLeft: "4px solid #cbd0d2",
      paddingLeft: "10px",
      color: "#17a2b8",
      fontWeight: "100"
    },
    uploadFile: {
      marginBottom: "20px"
    }
  };
};

class BackupsItem extends React.Component {
  state={
    itemLoading:false  
  }
  onFileChange = (backupId, fieldName, file) => {
    if (this.props.onFileChange) {
      this.setState({itemLoading:true})
      this.props.onFileChange(backupId, fieldName, file);
      //this.setState({itemLoading:false})
    }
  };
  handleDeleteBackupItem = () => {
    const { onDeleteClick, fieldProps } = this.props;
    const { backupId } = fieldProps;
    if (onDeleteClick) {
      onDeleteClick(backupId);
    }
  };  
 componentWillReceiveProps(newProps){
  let {loading} = newProps;
  if(!loading){
    this.setState({itemLoading:loading})
  }  
 }

  renderBackupItem = () => {
    const { classes, fieldProps = [], id, loading } = this.props;
    const { name, description = "", backupId, links, properties } = fieldProps;
    const { ...restProps } = properties;
    
    return (
      <Card className={classes.card}>
        <CardHeader
          className={classes.header}
          title="Backup File"
          action={
            <Button onClick={this.handleDeleteBackupItem} color="primary">
              Delete
            </Button>
          }
        />
        <CardContent className={classes.content}>
          <div className={classes.uploadFile}>
            {loading && this.state.itemLoading? (
              <ReactLoading type={"bubbles"} color={"#000000"} />
            ) : (
              <FormField
                id={backupId}
                label={"Add Backup"}
                onFileChange={this.onFileChange}
                allowComments={false}
                fieldType="FileUpload"
                value={name}
                {...restProps}
              />
            )}
          </div>
        </CardContent>
      </Card>
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid item className={classes.grid}>
        {this.renderBackupItem()}
      </Grid>
    );
  }
}

export default withStyles(styles)(BackupsItem);
