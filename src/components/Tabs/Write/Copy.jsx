import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import SectionHeader from "./SectionHeader";
import CopyItem from "./CopyItem";
import { bindActionCreators } from "redux";
import Modal from "../../Modal/Modal";
import Comments from "../../Comments";
import * as copy_actions from "../../../redux/actions/writeAction";
import "./../../../assets/scss/write.css";
const styles = theme => {
  return {
    root: {
      flexGrow: 1,
      margin: "10px"
    }
  };
};

class Copy extends React.Component {
  constructor() {
    super();
    this.state = {
      isModalOpen: false
    };
    this.saveInStore = this.saveInStore.bind(this);
  }

  saveInStore(payload) {
    this.props.updateCopyRequest(payload);
  }
  handleCommentModal = ({ open }) => {
    this.setState({
      isModalOpen: open
    });
  };
  render() {
    let { classes, copy } = this.props;
    if (!copy || !copy.copy) return null;
    return (
      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item xs={12}>
            <SectionHeader title="Article Copy" />
            <CopyItem
              fieldProps={copy.copy}
              onCommentBtnClick={this.handleCommentModal}
              saveInStore={this.saveInStore}
            />
            <Modal
              open={this.state.isModalOpen}
              onCloseClick={this.handleCommentModal}
            >
              <Comments />
            </Modal>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ write }) => {
  return {
    copy: write.write
  };
};
const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(copy_actions, dispatch)
  };
};
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(Copy);
export default withStyles(styles)(ConnectedComponent);
