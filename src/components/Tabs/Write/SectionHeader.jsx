import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Description";

const styles = theme => ({
  root: {
    marginTop: "20px"
  },
  messageWrapper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    backgroundColor: theme.palette.primary.light,
    flexGrow: 1,
    position: "relative"
  },
  icon: {
    marginLeft: -12,
    marginRight: 20,
    position: "absolute",
    top: "3px"
  },
  title: {
    flexGrow: 1,
    marginLeft: "36px"
  }
});

function SectionHeader(props) {
  const { classes, title = "" } = props;

  return (
    <div className={classes.root}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Paper className={classes.messageWrapper} elevation={1}>
          <IconButton className={classes.icon} color="inherit">
            <MenuIcon />
          </IconButton>
          <Typography variant="title" color="inherit" className={classes.title}>
            {title}
          </Typography>
        </Paper>
      </Grid>
    </div>
  );
}

export default withStyles(styles)(SectionHeader);
