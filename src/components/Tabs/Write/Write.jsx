import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Copy from "./Copy";
import Graphics from "./Graphics/Graphics";
import Backups from "./Backups/Backups";
import PropTypes from "prop-types";
import { fetchAllFieldsRequest } from "../../../redux/actions/writeAction";
import { unHideTabs } from "../../../redux/actions/dashBoardActions";
import { Toolbar, Typography, Button } from "@material-ui/core";
import { HashLink as Link } from "react-router-hash-link";
import scrollToComponent from "react-scroll-to-component";
import {toast} from 'react-toastify'
import * as R from "ramda";
const styles = theme => {
  return {
    root: {},
    heading: {
      fontWeight: 300,
      color: theme.palette.primary.main,
      fontSize: "28px"
    }
  };
};
class Write extends React.Component {
  constructor() {
    super();
    this.state = {
      isCommentModalOpen: false,
      modalOptions: null,
      commentFor: "",
      sections: []
    };
  }
  componentDidMount() {
    if (R.isEmpty(this.props.write.write))
      this.props.fetchAllFieldsRequest(this.props.assignment.id);
  }

  handleCommentModalHide = modalOptions => {
    this.setState({
      isCommentModalOpen: false,
      modalOptions
    });
  };
  handleCommentModal = label => {
    this.setState({
      isCommentModalOpen: true,
      commentFor: label
    });
  };
  handleBackupModal = () => {};
  onEditorChange = value => {
    console.log("editor value");
    console.log(value);
  };
  
  render() {
    const { classes, write, assignments } = this.props;
    if (write.error) return "Error from server";
    return (
      <div className={classes.root}>
        <Toolbar>
          <Typography variant="title" className={classes.heading}>
            Write
          </Typography>
        </Toolbar>
        <Toolbar>
          <Typography
            component={Button}
            onClick={() =>
              scrollToComponent(this.Copy, {
                offset: 0,
                align: "top",
                duration: 1500
              })
            }
          >
            Article Copy{" "}
          </Typography>
          <Typography component="span">|</Typography>
          <Typography
            component={Button}
            onClick={() =>
              scrollToComponent(this.Graphics, {
                offset: -150,
                align: "top",
                duration: 1500
              })
            }
          >
            Article Graphic
          </Typography>
          <Typography component="span">|</Typography>
          <Typography
            component={Button}
            onClick={() =>
              scrollToComponent(this.Backups, {
                offset: -150,
                align: "top",
                duration: 1500
              })
            }
          >
            Backup
          </Typography>
        </Toolbar>
        <Copy
          ref={section => {
            this.Copy = section;
          }}
        />
        <Graphics
          ref={section => {
            this.Graphics = section;
          }}
        />
        <Backups
          ref={section => {
            this.Backups = section;
          }}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchAllFieldsRequest: postId => dispatch(fetchAllFieldsRequest(postId)),
    unHideTabs: () => dispatch(unHideTabs())
  };
};
const mapStateToProps = ({ write, assignments }) => {
  return {
    write: write,
    assignment: assignments.assignment
  };
};

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Write);
export default withStyles(styles)(ConnectedComponent);
