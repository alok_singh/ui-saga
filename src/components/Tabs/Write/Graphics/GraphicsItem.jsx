import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Button, GridList, GridListTile } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import FormField from "../../../Form/FormField/FormField";
import ShortCode from "./ShortCode";
import ReactLoading from "react-loading";

const styles = theme => {
  return {
    root: {
      margin: "20px"
    },
    header: {
      display: "flex",
      flexDirection: "row"
    }
  };
};

class GraphicsItem extends React.Component {
  state={
    itemLoading:false  
  }
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.saveInStore = this.saveInStore.bind(this);
  }
  handleChange(event) {
    this.props.saveInStore({
      graphicsId: this.props.id,
      [event.target.name]: event.target.checked
    });
  }
  onFileChange = (graphicId, fieldName, file) => {
    if (this.props.onFileChange) {
      this.setState({itemLoading:true})
      this.props.onFileChange(graphicId, fieldName, file);
    }
  };
  handleDeleteGraphicItem = () => {
    const { onDeleteClick, id } = this.props;
    if (onDeleteClick) {
      onDeleteClick(id);
    }
  };
  componentWillReceiveProps(newProps){
  let {loading} = newProps;
  if(!loading){
this.setState({itemLoading:loading})
  }
  console.log(newProps);
 }
  renderForm = () => {
    const { fieldProps = [] } = this.props;
    console.log("Graphics item",fieldProps)
    const fieldData = fieldProps
      .filter(({ properties: { fieldType } }) => fieldType !== "CheckBox")
      .map((field, index) => {
        const { fieldName, properties, fieldId, fieldValue } = field;
        const { allowComments, ...restProps } = properties;
        return (
          <FormField
            key={index}
            label={fieldName}
            onBlur={this.saveInStore}
            allowComments={false}
            name={fieldId + ""}
            value={fieldValue}
            id={this.props.id}
            onFileChange={this.onFileChange}
            onCommentBtnClick={this.handleCommentModal}
            {...restProps}
          />
        );
      });
    return <div>{fieldData}</div>;
  };
  saveInStore(event) {
    this.props.saveInStore({
      graphicsId: this.props.id,
      [event.target.name]: event.target.value
    });
  }
  getCheckBox(fieldData) {
    const checkbox = fieldData.filter(data => data.fieldId == 6)[0];
    const { fieldName, properties, fieldId, fieldValue } = checkbox;
    const { allowComments, ...restProps } = properties;
    return (
      <FormField
        key={0}
        label={fieldName}
        handleChange={this.handleChange}
        name={fieldId + ""}
        value={fieldValue}
        {...restProps}
      />
    );
  }
  render() {
    const { classes, fieldProps, count,loading } = this.props;
    return (
      <div className={classes.root}>
        <div>
          <GridList cellHeight={90} className={classes.gridList} cols={4}>
            <GridListTile cols={1}>
              <div>
                <Typography variant="display1" component="span" gutterBottom>
                  {`Graphic #${count}`}
                  <Button
                    onClick={this.handleDeleteGraphicItem}
                    className={classes.button}
                  >
                    Delete
                  </Button>
                </Typography>
              </div>
            </GridListTile>
            <GridListTile cols={1}>{this.getCheckBox(fieldProps)}</GridListTile>
            <GridListTile cols={2}>
              <ShortCode count={count} />
            </GridListTile>
          </GridList>
        </div>
        {this.renderForm()}
      </div>
    );
  }
}

const ConnectedComponent = connect(null, null)(GraphicsItem);
export default withStyles(styles)(ConnectedComponent);
