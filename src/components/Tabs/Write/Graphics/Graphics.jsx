import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Grid from "@material-ui/core/Grid";
import GraphicsItem from "./GraphicsItem";
import AddGraphicsItem from "./AddGraphicsItem";
import SectionHeader from "../SectionHeader";
import { uploadFileApi } from "../../../../api/writeApi";
import * as write_graphic_actions from "../../../../redux/actions/writeAction";
import * as dashboard_load_actions from "../../../../redux/actions/dashBoardActions";
import _ from "lodash";
import ReactLoading from "react-loading";

const styles = theme => {
  return {
    root: {
      flexGrow: 1,
      margin: "10px"
    }
  };
};

class Graphics extends React.Component {
  state = {
    loading: false
  }
  constructor(props) {
    super(props);
    this.handleNewGraphicItem = this.handleNewGraphicItem.bind(this);
    this.saveInStore = this.saveInStore.bind(this);
  }
  saveInStore = payload => {
    //update graphics
    this.props.updateGraphicRequest(payload);
  };
  handleNewGraphicItem() {
    let newGraphic = this.props.store.write.graphics[0];
    if (newGraphic) {
      newGraphic = _.cloneDeep(newGraphic);
      Object.keys(newGraphic).forEach(graphic => {
        if (newGraphic[graphic]) {
          if (Array.isArray(newGraphic[graphic])) {
            newGraphic[graphic].forEach(field => {
              field.fieldValue = ""
            })
          } else {
            newGraphic[graphic] = ""
          }
        }
      })
      newGraphic.graphicsId = Date.now();
      this.props.showSuccessMsg("New Graphic Added")
      this.props.addGraphicsRequest(newGraphic); //state.write.graphics.push calling in writereducer
    }
  }
  onFileChange = (graphicId, fieldName, file) => {
    //const { graphic } = this.props;
    let assignmentId = this.props.store.write.assignment.percolateId;
    let userId = "INYMNB";
    this.setState({ loading: true })
    uploadFileApi({ file, url: '/graphics', assignmentId, userId }).then(({ data }) => {
      this.setState({ loading: false })
      this.props.showSuccessMsg("Successfully Uploaded")
      this.props.updateGraphicRequest({graphicsId: graphicId,[fieldName]: file.name});
    }).catch(error=>{
      this.setState({loading:false})
      this.props.showErrorMsg("File Upload Fail")
    })
  };
  handleDeleteGraphicsItem = graphicId => {
    const { store } = this.props;
    this.props.deleteGraphicsRequest({ graphicId, graphics:store.write.graphics });
  };
  renderItems = () => {
    const { classes, store } = this.props;
    if (Object.keys(store.write).length === 0) return "";
    return store.write.graphics
      .filter(graphic => graphic.graphicsId != "")
      .map((graphicItem, index) => {
        const count = ++index;
        const graphicField = graphicItem.fields[0];
        console.log(graphicItem);
        return (
          <GraphicsItem
            loading={this.state.loading}
            key={index}
            count={count}
            id={graphicItem.graphicsId}
            fileName={graphicItem.originalFileName}
            fieldProps={graphicItem.fields}
            onDeleteClick={this.handleDeleteGraphicsItem}
            onFileChange={this.onFileChange}
            saveInStore={this.saveInStore}
          />
        );
      });
  };
  render() {
    const { classes, store } = this.props;
    if (store.loading)
      return (
        <div>
          <ReactLoading type={"bubbles"} color={"#000000"} />
        </div>
      );
    return (
      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item xs={12}>
            <SectionHeader title="Article Graphics" />
            {this.renderItems()}
            <AddGraphicsItem onAddGraphicClick={this.handleNewGraphicItem} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    store: state.write,
    graphics: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(write_graphic_actions, dispatch)
  };
};

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(
  Graphics
);
export default withStyles(styles)(ConnectedComponent);
