import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const styles = theme => {
  return {
    grid: {
      display: "flex",
      flex: 1,
      alignItems: "center"
    },
    card: {
      height: "150px",
      display: "flex",
      flex: 1,
      backgroundColor: "#f9fafa",
      color: "#17a2b8",
      padding: "10px",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      marginBottom: "15px",
      cursor: "pointer"
    }
  };
};

class AddGraphicsItem extends React.Component {
  onClick = () => {
    if (this.props.onAddGraphicClick) {
      this.props.onAddGraphicClick();
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid item xs={8} className={classes.grid}>
        <Card className={classes.card} onClick={this.onClick}>
          <span>+</span>
          <span>Add New Graphic</span>
        </Card>
      </Grid>
    );
  }
}

export default withStyles(styles)(AddGraphicsItem);
