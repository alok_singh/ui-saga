import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const styles = theme => {
  return {
    shortcodeGrid: {},
    box: {
      border: "1px solid #16a2b8",
      padding: "10px",
      borderRadius: "10px",
      width: "95%"
    },
    shortcodeWrapper: {
      display: "flex",
      flexDirection: "row"
    },
    shortcodeBox: {
      border: "1px dashed #000",
      padding: "5px",
      borderRadius: "5px",
      marginRight: "5px"
    },
    button: {
      color: "#17a2b8"
    },
    margin: {
      margin: 0
    }
  };
};

class ShortCode extends React.Component {
  render() {
    const { classes, count } = this.props;
    return (
      <div className={classes.box}>
        <p className={classes.margin}>
          Places the graphics into your body using shortcode
        </p>
        <div className={classes.shortcodeWrapper}>
          <span className={classes.shortcodeBox}>
            {`[Graphic #${count} SHORTCODE ${count}]`}
          </span>
          <Button className={classes.button}>Copy Shortcode</Button>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ShortCode);
