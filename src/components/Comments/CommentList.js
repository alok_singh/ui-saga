import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import CommentItem from './CommentItem';
import CommentForm from './CommentForm';
import * as comment_action from '../../redux/actions/commentAction'
const styles = (theme) => {
  return {
    paper: {
      width: 300,
      display: 'flex',
      flexDirection: 'column',
      padding: '10px'
    }
  }
}

class CommentList extends React.Component {
  componentWillMount() {
    this.props.fetchCommentsRequest();
  }
  render() {
    const { classes, comments, targetText } = this.props;
    if(!comments.comment)return null;
    console.log(comments,"this.propsthis.props");
    const comment = {
      title: "AL",
      dateCreated: Date.now(),
      commentId: Date.now(),
      commentFor: "Body",
      commentTarget: "This is an example quotation that uses the blockquote tag.",
      replies: []
    }
    return (
      <div>
        {
          !comments[0] && <CommentItem
          comment={comment} />
        }
        {comments.comment.map((comment, index) => {
          return (
            <CommentItem key={index} comments={comments} comment={comment} />
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    comments:state.comment
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(comment_action, dispatch)
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(CommentList);
export default withStyles(styles)(ConnectedComponent);
