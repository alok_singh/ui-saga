import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ReplyItem from './ReplyItem';

const styles = (theme) => {
  return {
  }
}

class ReplyList extends React.Component {
  renderReplies = () => {
    const { replies = [] } = this.props;
    return replies.map(({replyText}, index) => (
      <ReplyItem key={index} text={replyText} />
    ));
  }
  render() {
    const { classes } = this.props;
    return (
      <div>{this.renderReplies()}</div>
    );
  }
}
export default withStyles(styles)(ReplyList);
