import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => {
  return {
    actions: {
      display: 'flex'
    }
  }
}

class CommentForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      commentText: ''
    }
  }
  onChange = (e) => {
    this.setState({
      commentText: e.target.value
    });
  }
  onKeyUp = (e) => {
    const { commentId, onCommentAdd } = this.props;
    if (e.which === 13) {
      if (onCommentAdd) {
        onCommentAdd(this.state.commentText, commentId);
        this.setState({
          commentText: ''
        });
      } else {
        console.error('onCommentAdd is not defined!');
      }
    }
  }
  render() {
    const { classes, actions, inputProps, type, commentId } = this.props;
    return (
      <CardActions className={classes.actions} disableActionSpacing>
        <TextField
          InputProps={{
            disableUnderline: true,
            ...inputProps
          }}
          type={type}
          placeholder="Reply..."
          fullWidth
          value={this.state.commentText}
          onChange={this.onChange}
          onKeyUp={this.onKeyUp}
        />
      </CardActions>
    );
  }
}

export default withStyles(styles)(CommentForm);
