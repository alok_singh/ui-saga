import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import CommentHeader from './CommentHeader';
import CommentTarget from './CommentTarget';
import CommentForm from './CommentForm';
import ReplyList from './ReplyList';
import moment from 'moment';

const styles = theme => ({
  card: {
    width: 300,
    margin: '15px 0'
  },
  cardContent: {
    flexGrow: 1
  }
});

class CommentItem extends React.Component {
  handleAddReply = (replyText, commentId) => {
    const { comments, comment } = this.props;
    const reply = {
      commentId,
      replyText
    };
    
    // this.props.addReply(comments, comment, reply);
  }
  render() {
    const {classes,comment} = this.props;
    let {commentTarget,replies,title,dateCreated,commentId} = comment;

    return (
      <div>
        <Card className={classes.card}>
          <CommentHeader title={title} subheader={moment(dateCreated).startOf('second').fromNow()} />
          <CardContent className={classes.cardContent}>
            <CommentTarget targetText={commentTarget} />
            <ReplyList replies={replies} />
          </CardContent>
          <Divider />
          <CommentForm commentId={commentId} onCommentAdd={this.handleAddReply} />
        </Card>
      </div>
    );
  }
}

const mapStateToProps = ({comments_reducer}) => {
  return {...comments_reducer};
}

const mapDispatchToProps = (dispatch) => {
  return {
    // ...bindActionCreators(actions, dispatch)
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(CommentItem);
export default withStyles(styles)(ConnectedComponent);
