import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import CommentControls from '../Comments/CommentControls';

const styles = (theme) => {
  return {
  }
}

const CommentHeader = (props) => {
  const { classes, title, subheader } = props;
  return (
    <CardHeader
      action={<CommentControls />}
      title={title}
      subheader={subheader}
    />
  );
}

export default withStyles(styles)(CommentHeader);
