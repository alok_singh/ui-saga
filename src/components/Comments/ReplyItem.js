import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => {
  return {
    root: {
      padding: '10px',
      backgroundColor: '#EEEEEE',
      marginBottom: '2px'
    }
  }
}

const ReplyItem = (props) => {
  const { classes, text } = props;
  return (
    <Typography component="p" className={classes.root}>
      {text}
    </Typography>
  );
}

export default withStyles(styles)(ReplyItem);
