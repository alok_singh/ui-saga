import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => {
  return {
    blockquote: {
      borderLeft: '3px solid #cbd0d2',
      paddingLeft: '10px',
      color: '#cbd0d2',
      fontWeight: '100'
    }
  }
}

const CommentTarget = (props) => {
  const { classes, targetText, title } = props;
  return (
    <div>
      {title ? <Typography variant="caption" gutterBottom>
        {title}
      </Typography> : ""}
      <blockquote className={classes.blockquote}>
        {targetText}
      </blockquote>
    </div>
  );
}

export default withStyles(styles)(CommentTarget);
